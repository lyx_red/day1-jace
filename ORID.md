# ORID



## Objective

- There was a standup meeting first this morning, summarize what you did last Friday and share any problems or suggestions you had.
- Today began the first day of ITA's formal training, I learned about ITA's specific training content and the arrangement for the next month, participated in the ice breaking game in the morning, and shared their expectations during the training. At the same time, we watched a video about how to learn.
- In the afternoon, I mainly learned the knowledge of concept map, what is concept map, how to make concept map and so on. At the same time, our team drew a concept map for a focal problem, which strengthened our team's collaboration ability. At the same time, the teacher introduced us to the concept of "ORID" and the standup meeting.



## Reflective

I feel very happy and looking forward to it.

## Interpretative

Because I not only learned new knowledge, but also finished the homework with my classmates, which made me feel the bond between us became deeper.

## Decision

I will continue to learn more knowledge from the teachers in the following time. At the same time, I will continue to communicate with my friends and finish more homework together.
